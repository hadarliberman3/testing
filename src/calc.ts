

export function sum(...numbers:number[]){
    return [...numbers].reduce((prev,cur)=>prev+=cur,0);
}

export function multiply(multiplier:number, [...numbers]:number[]){
    return [...numbers].map(cur=>cur*multiplier);

}

export function delay(ms:number){
    return new Promise((resolve)=>setTimeout((resolve),ms));
}