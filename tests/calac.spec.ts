import {expect} from "chai";
import {sum,multiply,delay} from "../src/calc";

describe("The calc module", function() {
     it("should exists",  ()=> {
        expect(sum).to.be.a("function");
        expect(sum).to.be.instanceOf(Function);
    });

      it("should sum of 2 numbers",async ()=> {
        await delay(1000);
        const mySum= await sum(1,2);
        expect(mySum).to.equal(3);
    
    });
    it("should sum of many numbers", ()=> {
        const mySum=sum(1,2,3,4);
        expect(mySum).to.equal(10);
    
    });
    it("should exists", ()=> {
        expect(multiply).to.be.a("function");
        expect(multiply).to.be.instanceOf(Function);
    });

      it("should multiply the elements of the array with the multiplier", ()=> {
        const mymultiply=multiply(2,[1,2]);
        expect(mymultiply).to.eql([2,4]);
    
    });
    it("should multiply many numbers", ()=> {
        const mymultiply=multiply(2,[2,3,4,3]);
        expect(mymultiply).to.eql([4,6,8,6]);
    
    });
  });